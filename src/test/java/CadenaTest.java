import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class CadenaTest {

	Cadena cadena = new Cadena();
	
	@Test
	void longitudTest() {
		int salida = 5;
		String entrada = "Adi�s";
		assertEquals(salida, this.cadena.longitud(entrada), "Longitud de la cadena");
	}
	
	@Test
	void longitudTestFail() {
		int salida = 12;
		String entrada = "Adi�s";
		assertEquals(salida, this.cadena.longitud(entrada), "Longitud de la cadena");
	}
	
	@Test
	void vocalesTest() {
		int salida = 2;
		String entrada = "Arroba";
		assertEquals(salida, this.cadena.vocales(entrada), "Vocales de la cadena");
	}
	
	@Test
	void vocalesTestFail() {
		int salida = 3;
		String entrada = "Arroba";
		assertEquals(salida, this.cadena.vocales(entrada), "Vocales de la cadena");
	}
	
	@Test
	void invertirTest() {
		String salida = "zorrA"; // Animal
		String entrada = "Arroz";
		assertEquals(salida, this.cadena.invertir(entrada), "Cadena pero invertida");
	}
	
	@Test
	void invertirTestFail() {
		String salida = "Sebasti�n";
		String entrada = "I�aki";
		assertEquals(salida, this.cadena.invertir(entrada), "Cadena pero invertida");
	}
	
	@Test
	void contarLetraTest() {
		int salida = 3; // Animal
		String entrada1 = "Esternocleidooccipitomastoideo";
		char entrada2 = 'c';
		assertEquals(salida, this.cadena.contarLetra(entrada1, entrada2), "Cadena pero invertida");
	}
	
	@Test
	void contarLetraFail() {
		int salida = 2345678;
		String entrada1 = "Peaky Blinders";
		char entrada2 = 'e';
		assertEquals(salida, this.cadena.contarLetra(entrada1, entrada2), "Cadena pero invertida");
	}

}
