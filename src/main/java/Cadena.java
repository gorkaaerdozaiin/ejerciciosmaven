
public class Cadena {
	
	public int longitud(String cadena) {
		return cadena.length();
	}
	
	public int vocales(String cadena) {
		int count = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'e' || cadena.charAt(i) == 'i' || cadena.charAt(i) == 'o' || cadena.charAt(i) == 'u') {
				count++;
			}
		}
		return count;
	}
	
	public String invertir(String cadena) {
		String invertida = "";
		for (int i = cadena.length() - 1; i >= 0; i--) {
			invertida += cadena.charAt(i);
		}
		return invertida;
	}
	
	public int contarLetra(String cadena, char caracter) {
		int count = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if (caracter == cadena.charAt(i)) {
				count++;
			}
		}
		return count;
	}
	
}
